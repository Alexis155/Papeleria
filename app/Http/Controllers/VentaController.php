<?php

namespace App\Http\Controllers;

use App\Models\DetalleVenta;
use App\Models\Producto;
use App\Models\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class VentaController extends Controller
{
    public function index() {
        $total = 0;
        foreach ($this->obtenerProductos() as $producto) {
            $total += $producto->existencia * $producto->precio;
        }
        return view('ventas.index',['total' => $total]);
    }

    public function store(Request $request){
        $descuento = 0;
        $porcentaje = 0;
        $venta = Venta::create([
            'fecha' => date('Y-m-d H:i:s'),
            'total' => 0,
            'descuento' => $descuento,
            'porcentaje' => $porcentaje,
            'status' => 1
        ]);

        $productos = $this->obtenerProductos();
        $total = 0;
        foreach ($productos as $producto) {
            $detalle = new DetalleVenta();
            $detalle->fill([
                'venta_id' => $venta->id,
                'producto_id' => $producto->id,
                'cantidad' => $producto->existencia
            ]);
            $detalle->saveOrFail();
            $total_producto = $producto->precio * $producto->existencia;
            $total += $total_producto;
            $productoActualizado = Producto::find($producto->id);
            $productoActualizado->existencia -= $detalle->cantidad;
            $productoActualizado->saveOrFail();
        }
        if($request->desc){
            $total -= $request->desc;
            $descuento = $request->desc;
            $porcentaje = $request->descuento;
        }
        $this->vaciarProductos();
        $venta->update([
            'total' => $total,
            'descuento' => $descuento,
            'porcentaje' => $porcentaje,
            'status' => 3,
        ]);
        return redirect()->route('venta.index')->with('message', 'Venta Completa');
    }

    public function show($id)
    {
        $detalle = DetalleVenta::with('producto')->where('venta_id', $id)->get();
        return response()->json($detalle);
    }

    public function reportes(Request $request){
        $mes = explode('-',$request->inicio);

        $total = Venta::all()->sum('total');
        $ventas = Venta::all();
        if($request->inicio){
            $ventas = Venta::whereMonth('created_at',$mes[1])->get();
            $total = Venta::whereMonth('created_at',$mes[1])->sum('total');
        }
        $data = [
            'ventas' => $ventas,
            'total' => $total
        ];
        return view('ventas.reportes')->with($data);
    }

    public function buscarProductoVenta(Request $request){
        $codigo = $request->post('codigo');
        $producto = Producto::where('codigo', '=', $codigo)->first();
        if (!$producto) {
            return redirect()
                ->route('venta.index')->with('error', 'Producto no encontrado');
        }
        $this->agregar($producto);
        return redirect()->route('venta.index')->with('producto',$producto);
    }
    private function agregar($producto){
        if($producto->existencia <= 0){
            return redirect()->back()->with('error', 'Producto sin existencias');
        }
        $productos = $this->obtenerProductos();
        $posibleIndice = $this->buscarIndiceDeProducto($producto->codigo, $productos);
        if($posibleIndice == -1){
            $producto->existencia = 1;
            array_push($productos, $producto);
        }else{
            if($productos[$posibleIndice]->existencia + 1 > $producto->existencia){
                return redirect()->back()->with('error', 'No hay mas productos para agregar');
            }
            $productos[$posibleIndice]->existencia += 1;

        }
        $this->guardarProductosVenta($productos);
    }

    private function obtenerProductos(){
        $productos = session("productosVenta");
        if (!$productos) {
            $productos = [];
        }
        return $productos;
    }

    public function agregarProductoVenta(Request $request){
        $producto = Producto::find($request->id);
        if($producto->existencia <= 0){
            return redirect()->back()->with('error', 'Producto sin existencias');
        }
        if($producto->existencia < $request->cantidad){
            return redirect()->back()->with('error', 'No hay suficientes productos, intentar con una cantidad menor');
        }
        $productos = $this->obtenerProductos();
        $posibleIndice = $this->buscarIndiceDeProducto($producto->codigo, $productos);
        if($posibleIndice == -1){
            $producto->existencia = $request->cantidad != null ? $request->cantidad : 1;
            array_push($productos, $producto);
        }else{
            if($productos[$posibleIndice]->existencia + 1 > $producto->existencia || $productos[$posibleIndice]->existencia + $request->cantidad > $producto->existencia){
                return redirect()->back()->with('error', 'No hay mas productos para agregar');
            }
            $productos[$posibleIndice]->existencia += $request->cantidad != null ? $request->cantidad : 1;

        }
        $this->guardarProductosVenta($productos);
        return redirect()->route('venta.index')->with('producto',$producto);
    }

    private function guardarProductosVenta($productos){
        session(["productosVenta" => $productos,
        ]);
    }

    private function buscarIndiceDeProducto(string $codigo, array &$productos){
        foreach ($productos as $indice => $producto) {
            if ($producto->codigo === $codigo) {
                return $indice;
            }
        }
        return -1;
    }

    public function quitarProductoVenta(Request $request){
        $indice = $request->post("indice");
        $productos = $this->obtenerProductos();
        array_splice($productos, $indice, 1);
        if(sizeof($productos) <= 0){
            $this->vaciarProductos();
        }else{
            $this->guardarProductosVenta($productos);
        }
        return redirect()->route('venta.index');
    }

    private function vaciarProductos(){
        $this->guardarProductosVenta(null);
    }
}
