<?php

namespace App\Http\Controllers;

use App\Models\Entrada;
use App\Models\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Array_;

class HomeController extends Controller
{
    public function index()
    {
        DB::select('SET lc_time_names = es_ES');
        $mes = DB::select('SELECT monthname(now()) as mes');
        $totalCompras = Entrada::whereMonth('created_at', now()->month)->sum('total');
        $totalVentas = Venta::whereMonth('created_at', now()->month)->sum('total');

        // $productosmasvendidos = DB::select('SELECT p.codigo as code, sum(dv.cantidad) as cantidad, p.nombre as nombre, p.id as id, p.existencia as stock
        // from productos p inner join detalle_ventas dv on p.id = dv.producto_id
        // inner join ventas v on dv.venta_id = v.id where year(v.created_at) = year(curdate()) group by p.codigo, p.nombre, p.id, p.existencia
        // order by sum(dv.cantidad) desc limit 10');

        $data = [
            'mes' => $mes[0]->mes,
            'totalCompras' => $totalCompras,
            'totalVentas' => $totalVentas
        ];
        return view('home')->with($data);
    }
}
