<?php

namespace App\Http\Controllers;

use App\Models\DetalleEntrada;
use App\Models\Entrada;
use App\Models\Producto;
use Illuminate\Http\Request;

class EntradasController extends Controller
{
    public function index(){
        $total = 0;
        foreach ($this->obtenerProductos() as $producto) {
            $total += $producto->existencia * $producto->precio;
        }
        return view('entradas.index',['total' => $total]);
    }

    public function store(Request $request){
        $entrada = Entrada::create([
            'fecha' => date("Y-m-d H:i:s"),
            'total' => 00.00,
            'status' => 1
        ]);
        $productos = $this->obtenerProductos();
        $total = 0;
        foreach ($productos as $producto) {
            $detalle = new DetalleEntrada();
            $detalle->fill([
                'entrada_id' => $entrada->id,
                'producto_id' => $producto->id,
                'costo_unitario' => $producto->precio,
                'cantidad' => $producto->existencia
            ]);
            $detalle->saveOrFail();
            $total_producto = $producto->precio * $producto->existencia;
            $total = $total + $total_producto;
            $productoActualizado = Producto::find($producto->id);
            $productoActualizado->existencia += $detalle->cantidad;
            $productoActualizado->saveOrFail();
        }
        $this->vaciarProductos();
        $entrada->update([
            'total' => $total,
            'status' => 3,
        ]);
        return redirect()->route('home')->with('message', 'Registro completo');
    }

    public function reportes(){
        $compras = Entrada::all();
        $data = [
            'entradas' => $compras
        ];
        return view('entradas.reportes')->with($data);
    }

    public function show($id){
        $producto = Producto::where('codigo','=',$id)->get();
        return response()->json($producto[0]);
    }

    public function buscarProducto(Request $request){
        $codigo = $request->post('codigo');
        $producto = Producto::where('codigo', '=', $codigo)->first();
        if (!$producto) {
            return redirect()
                ->route('entradas.index')->with('error', 'Producto no encontrado');
        }
        return redirect()->route('entradas.index')->with('producto',$producto);
    }

    private function obtenerProductos(){
        $productos = session("productos");
        if (!$productos) {
            $productos = [];
        }
        return $productos;
    }

    public function agregarProducto(Request $request){
        $producto = Producto::find($request->id);
        $producto->existencia = $request->cantidad;
        $producto->precio = $request->precio;
        $productos = $this->obtenerProductos();
            array_push($productos, $producto);
        $this->guardarProductos($productos);
        return redirect()->route('entradas.index');
    }

    private function guardarProductos($productos){
        session(["productos" => $productos,
        ]);
    }

    public function quitarProducto(Request $request){
        $indice = $request->post("indice");
        $productos = $this->obtenerProductos();
        array_splice($productos, $indice, 1);
        if(sizeof($productos) <= 0){
            $this->vaciarProductos();
        }else{
            $this->guardarProductos($productos);
        }
        return redirect()->route('entradas.index');
    }

    private function vaciarProductos(){
        $this->guardarProductos(null);
    }

}
