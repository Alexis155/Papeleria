<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'categorias' =>  Categoria::all(),
            'productos' => Producto::with('categoria')->get()
        ];
        return view('producto\index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:50',
            'codigo' => 'required',
            'existencia' => 'required|numeric|min:1',
            'precio' => 'required',
            'descripcion' => 'required'
        ]);
        $producto = Producto::create([
            'codigo' => $request->codigo,
            'nombre' => $request->nombre,
            'existencia' =>  $request->existencia,
            'precio' => $request->precio,
            'descripcion' =>  $request->descripcion,
            'estatus' => 1,
            'categoria_id' => $request->categoria_id
        ]);
        return back()->with('message', 'Producto reguistrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::find($id);
        return response()->json($producto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->update([
            'nombre' => $request->nombre,
            'existencia' =>  $request->existencia,
            'precio' => $request->precio,
            'descripcion' =>  $request->descripcion,
            'categoria_id' => $request->categoria_id
        ]);
        // return back()->with('message', 'Producto actualizado correctamente');
        return response()->json([
            'producto' => $producto,
            'type' => 'success',
            'message' => 'Producto actualizado con exito'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
