<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrada extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha', 'total', 'status'
    ];
    public function detalle()
    {
        return $this->hasMany(DetalleEntrada::class);
    }
}
