<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleEntrada extends Model
{
    use HasFactory;

    protected $fillable = [
        'entrada_id', 'producto_id', 'costo_unitario', 'cantidad',
    ];

    public function entrada()
    {
        return $this->belongsTo(Entrada::class);
    }
}
