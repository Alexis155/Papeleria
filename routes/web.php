<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the 'web' middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');
Route::resource('productos', 'App\Http\Controllers\ProductoController');
Route::resource('categoria', 'App\Http\Controllers\CategoriaController');
Route::resource('venta', 'App\Http\Controllers\VentaController');
Route::get('/reporteVentas', 'App\Http\Controllers\VentaController@reportes')->name('reporteVenta');
Route::post('/buscarProductoVenta', 'App\Http\Controllers\VentaController@buscarProductoVenta')->name('buscarProductoVenta');
Route::post('/agregarProductoVenta', 'App\Http\Controllers\VentaController@agregarProductoVenta')->name('agregarProductoVenta');
Route::delete('/quitarProductoVenta', 'App\Http\Controllers\VentaController@quitarProductoVenta')->name('quitarProductoVenta');
Route::resource('entradas', 'App\Http\Controllers\EntradasController');
Route::get('/reporteCompras', 'App\Http\Controllers\EntradasController@reportes')->name('reporteCompra');
Route::post('/buscarProducto', 'App\Http\Controllers\EntradasController@buscarProducto')->name('buscarProducto');
Route::post('/agregarProducto', 'App\Http\Controllers\EntradasController@agregarProducto')->name('agregarProducto');
Route::delete('/quitarProducto', 'App\Http\Controllers\EntradasController@quitarProducto')->name('quitarProducto');

