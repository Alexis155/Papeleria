@extends('layout\app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
@endpush

@section('header')
<div class="mx-5 ">
    <div class="row mb-2">
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-start">
                <li class="breadcrumb-item">
                    <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active">
                   Productos <i class="fas fa-boxes"></i>
                </li>
            </ol>
        </div>
        <div class="col-sm-6 text-end mt-3">
            <a href="/"><button class="btn btn-outline-dark btn-lg"> <i class="fas fa-arrow-left"></i> Regresar</button></a>
        </div>
    </div>
    <hr>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8 pb-3">
        <div class="card">
            <div class="card-header text-center bg-title">
                <h1 class="fs-4 fw-bold">Lista de Productos</h1>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-productos">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Existencia</th>
                                <th>Precio</th>
                                <th>Categoria</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                            <tr>
                                <td>{{ $producto->codigo }}</td>
                                <td>{{ $producto->nombre }}</td>
                                <td>{{ $producto->existencia }}</td>
                                <td>$ {{ $producto->precio }}</td>
                                <td>{{ $producto->categoria->nombre }}</td>
                                <td class="text-end">
                                    <button type="button" class="btn btn-outline-dark edit" data-toggle="modal" data-target="#EditModal" data-id="{{ $producto->id }}"><i class="fas fa-edit"></i> Editar</button>
                                    <a href="#" class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i> Eliminar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Existencia</th>
                                <th>Precio</th>
                                <th>Categoria</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 pb-3 ">
        <div class="card">
            <div class="card-header text-center bg-title">
                <h1 class="fs-4 fw-bold">Registrar Productos</h1>
            </div>
            <form action="{{ route('productos.store') }}" method="POST" class="form-group">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Codigo</label>
                                <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Ingresar Codigo" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresar Nombre" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="precio">Precio</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control" id="precio" name="precio">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Existencia</label>
                                <input type="number" min="0" class="form-control" id="existencia" name="existencia" placeholder="Existencia" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Descripcion</label>
                                <input type="text" min="0" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Categoria</label>
                                <select name="categoria_id" id="categoria_id" class="form-control" required>
                                    @foreach ($categorias as $categoria)
                                        <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar Producto</h5>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Codigo</label>
                                <input type="text" class="form-control" id="codigoM" name="codigoM" placeholder="Ingresar Codigo" disabled>
                                <input type="text" name="id" id="id" hidden>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Nombre</label>
                                <input type="text" class="form-control" id="nombreM" name="nombreM" placeholder="Ingresar Nombre" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="precio">Precio</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control" id="precioM" name="precioM">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Existencia</label>
                                <input type="number" min="0" class="form-control" id="existenciaM" name="existenciaM" placeholder="Existencia" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Descripcion</label>
                                <input type="text" min="0" class="form-control" id="descripcionM" name="descripcionM" placeholder="Descripcion" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Categoria</label>
                                <select name="categoria_idM" id="categoria_idM" class="form-control" required>
                                    @foreach ($categorias as $categoria)
                                        <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success update"><i class="far fa-save"></i> Guardar cambios</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
   <script src="{{ asset('js/datatables.min.js') }}"></script>
   <script>
       $(document).ready(function(){
        $('.dataTables-productos').DataTable();
        $('.edit').click(function(event){
            event.preventDefault();
            let _id = $(this).data('id');
            $.ajax({
                url:'{{ url('productos') }}/'+_id,
                type: 'GET',
                dataType: 'json',
            }).done(function(data){
                console.log(data);
                $('#id').val(data.id);
                $('#codigoM').val(data.codigo);
                $('#nombreM').val(data.nombre);
                $('#precioM').val(data.precio);
                $('#existenciaM').val(data.existencia);
                $('#descripcionM').val(data.descripcion);
                $('#categoria_idM').val(data.categoria_id);
            })
        });
        $('.update').click(function(){
            let _id = $('#id').val();
            let _data = {
                nombre :$('#nombreM').val(),
                precio :$('#precioM').val(),
                existencia :$('#existenciaM').val(),
                descripcion :$('#descripcionM').val(),
                categoria_id :$('#categoria_idM').val(),
            }
            $.ajax({
                url:'{{ url('productos') }}/'+_id,
                type: 'PUT',
                dataType: 'json',
                data: _data,
            }).done(function(data){
                $('#EditModal').modal('hide');
                Command: toastr[data.type](data.message)
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                }
                setTimeout(function(){ location.reload() }, 3000);
            });
        });
    });
   </script>
@endpush
@endsection
