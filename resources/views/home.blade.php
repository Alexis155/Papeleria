@extends('layout.app')

@push('styles')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endpush

@section('header')
<div class="mx-5 ">
    <div class="row">
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-start">
                <li class="breadcrumb-item">
                   <p>Inicio</p>
                </li>
                <li class="breadcrumb-item active">
                   Papeleria Diana <i class="fas fa-home"></i>
                </li>
            </ol>
        </div>
        <hr>
    </div>
</div>
@endsection

@section('content')

<section>
    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-6">
                <div class="card text-center border-3 border-info text-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 d-contents">
                                    <h1><i class="fas fa-cash-register"></i></h1>
                            </div>
                            <div class="col-10 text-start">
                                <h3>Ventas del mes de {{ $mes }}</h3>
                                <h2>$ {{ number_format($totalVentas, 2) }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card border-3 border-info text-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 d-contents">
                                <h1><i class="fas fa-shopping-bag"></i></h1>
                            </div>
                            <div class="col-10 text-start">
                                <h3>Compras del mes de {{ $mes }}</h3>
                                <h2>$ {{ number_format($totalCompras, 2) }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>

<section>
    <div class="container px-lg-5">
        <!-- Page Features-->
        <div class="row gx-lg-5">
            <div class="col-lg-4 col-xxl-4 mb-5">
                <div class="card bg-fondo border-0 h-100">
                    <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div class="feature bg-button bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="fas fa-cash-register"></i></div>
                        <a href="{{ route('venta.index') }}"><h2 class="fs-4 fw-bold">Vender</h2></a>
                        <p class="mb-0">Realizar ventas!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xxl-4 mb-5">
                <div class="card bg-fondo border-0 h-100">
                    <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div class="feature bg-button bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="fas fa-shopping-bag"></i></div>
                        <a href="{{ route('entradas.index') }}"><h2 class="fs-4 fw-bold">Entradas</h2></a>
                        <p class="mb-0">Registrar entradas de prouctos</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xxl-4 mb-5">
                <div class="card bg-fondo border-0 h-100">
                    <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div class="feature bg-button bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="fas fa-boxes"></i></div>
                        <a href="{{ route('productos.index') }}"><h2 class="fs-4 fw-bold">Productos</h2></a>
                        <p class="mb-0">Reguistrar Productos</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xxl-4 mb-5">
                <div class="card bg-fondo border-0 h-100">
                    <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div class="feature bg-button bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="fas fa-th-list"></i></div>
                        <a href="{{ route('categoria.index') }}"><h2 class="fs-4 fw-bold">Categorias</h2></a>
                        <p class="mb-0">Gestionar Categorias</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xxl-4 mb-5">
                <div class="card bg-fondo border-0 h-100">
                    <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div class="feature bg-button bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="fas fa-paste"></i></div>
                        <a href="{{ route('reporteVenta') }}"><h2 class="fs-4 fw-bold">Reporte de Ventas</h2></a>
                        <p class="mb-0">Gestionar Categorias</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xxl-4 mb-5">
                <div class="card bg-fondo border-0 h-100">
                    <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                        <div class="feature bg-button bg-gradient text-white rounded-3 mb-4 mt-n4"><i class="fas fa-paste"></i></div>
                        <a href="{{ route('reporteCompra') }}"><h2 class="fs-4 fw-bold">Reporte de Compras</h2></a>
                        <p class="mb-0">Gestionar Categorias</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
@endpush
@endsection
