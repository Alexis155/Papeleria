@extends('layout\app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}"> --}}
@endpush

@section('header')
<div class="mx-5">
    <div class="row mb-2">
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-start">
                <li class="breadcrumb-item">
                    <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active">
                   Categorias <i class="fas fa-th-list"></i>
                </li>
            </ol>
        </div>
        <div class="col-sm-6 text-end mt-3">
            <a href="/"><button class="btn btn-outline-dark btn-lg"> <i class="fas fa-arrow-left"></i> Regresar</button></a>
        </div>
    </div>
    <hr>
</div>
@endsection

@section('content')
<!-- Header-->
<div class="row">
    <div class="col-md-8 pb-0">
        <div class="card">
            <div class="card-header text-center bg-title">
                <h1 class="fs-4 fw-bold">Lista de categorias</h1>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-categorias text-center" >
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categorias as $categoria)
                                <tr>
                                    <td>{{ $categoria->nombre }}</td>
                                    <td>
                                        {{-- <a href="#" class="btn btn-outline-dark"><i class="fas fa-edit"></i> Editar</a> --}}
                                        <a href="#" class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i> Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 pb-3">
        <div class="card">
            <div class="card-header text-center bg-title">
                <h1 class="fs-4 fw-bold">Registrar Categoria</h1>
            </div>
            <form action="{{ route('categoria.store') }}" method="POST" class="form-group">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresar Nombre">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
   <script src="{{ asset('js/datatables.min.js') }}"></script>
   <script>
       $(document).ready(function(){
        $('.dataTables-categorias').DataTable();
    });
   </script>
@endpush
@endsection
