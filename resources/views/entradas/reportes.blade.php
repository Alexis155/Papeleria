@extends('layout.app')

@push('styles')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endpush

@section('header')
    <div class="mx-5">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-start">
                    <li class="breadcrumb-item">
                        <a href="/">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active">
                    Reporte de compras <i class="fas fa-paste"></i>
                    </li>
                </ol>
            </div>
            <div class="col-sm-6 text-end mt-3">
                <a href="/"><button class="btn btn-outline-dark btn-lg"> <i class="fas fa-arrow-left"></i> Regresar</button></a>
            </div>
        </div>
        <hr>
    </div>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12 pb-3 pt-3">
            <div class="card">
                <div class="card-header text-center bg-title">
                    <h1 class="fs-4 fw-bold">Reporte de compras</h1>
                </div>
                <div class="card-boy">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-compras">
                            <thead>
                                <tr>
                                    <th>Fecha de compra</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($entradas as $entrada)
                                <tr>
                                   <td>{{ $entrada->created_at }}</td>
                                   <td>{{ $entrada->total }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Fecha de compra</th>
                                    <th>Total</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-end"></div>
            </div>
        </div>
    </div>
    @push('scripts')
   <script src="{{ asset('js/datatables.min.js') }}"></script>
   <script>
       $(document).ready(function(){
        $('.dataTables-compras').DataTable();
    });
   </script>
@endpush
@endsection
