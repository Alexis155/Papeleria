@extends('layout.app')

@push('styles')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endpush

@section('header')
<div class="mx-5">
    <div class="row mb-2">
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-start">
                <li class="breadcrumb-item">
                    <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active">
                   Mercancia <i class="fas fa-shopping-bag"></i>
                </li>
            </ol>
        </div>
        <div class="col-sm-6 text-end mt-3">
            <a href="/"><button class="btn btn-outline-dark btn-lg"> <i class="fas fa-arrow-left"></i> Regresar</button></a>
        </div>
    </div>
    <hr>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 pb-3 pt-3">
        <div class="card">
            <div class="card-header text-center bg-title">
                <h1 class="fs-4 fw-bold">Capturar compras de mercancía</h1>
            </div>
            <div class="card-body">
                <div class="col-12 col-md-12">
                    <form action="{{route("buscarProducto")}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="codigo">Código de barras</label>
                            <input id="codigo" autocomplete="off" required autofocus name="codigo" type="text" class="form-control" placeholder="Código de barras">
                        </div>
                    </form>
                    @if (Session::has('producto'))
                        <form class="form-group" action="{{ route('agregarProducto') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="codigo">Código de barras</label>
                                        <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Ingresar codigo de barras" value="{{ session('producto')->codigo }}" disabled>
                                        <input type="text" class="form-control" id="id" name="id" value="{{ session('producto')->id }}" hidden>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ session('producto')->nombre }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="cantidad">Cantidad</label>
                                        <input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad a registrar" autofocus required>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label for="precio">Precio Unitario</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="text" class="form-control" id="precio" name="precio" required autofocus>
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3 text-end">
                                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Agregar</button>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
                @if (Session::has('productos'))
                    <div class="col-md-12 mt-5">
                        <h2>Total: ${{number_format($total, 2)}}</h2>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Código de barras</th>
                                        <th>Descripción</th>
                                        <th>Precio</th>
                                        <th>Cantidad</th>
                                        <th>Quitar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(session("productos") as $producto)
                                        <tr>
                                            <td>{{ $producto->codigo }}</td>
                                            <td>{{ $producto->nombre }}</td>
                                            <td>$ {{ $producto->precio }}</td>
                                            <td>{{ $producto->existencia }}</td>
                                            <td>
                                                <form action="{{route("quitarProducto")}}" method="post">
                                                    @method("delete")
                                                    @csrf
                                                    <input type="hidden" name="indice" value="{{$loop->index}}">
                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
            <div class="card-footer text-end">
                @if (Session::has('productos'))
                    <form action="{{ route('entradas.store') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Terminar registro</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
