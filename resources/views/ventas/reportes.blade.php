@extends('layout.app')

@push('styles')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
@endpush

@section('header')
    <div class="mx-5">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-start">
                    <li class="breadcrumb-item">
                        <a href="/">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active">
                    Reporte de ventas <i class="fas fa-paste"></i>
                    </li>
                </ol>
            </div>
            <div class="col-sm-6 text-end mt-3">
                <a href="/"><button class="btn btn-outline-dark btn-lg"> <i class="fas fa-arrow-left"></i> Regresar</button></a>
            </div>
        </div>
        <hr>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="container">
                <div class="card text-center border-3 border-info text-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <h1><i class="fas fa-cash-register"></i>Total de ventas</h1>
                                <h2>$ {{ number_format($total, 2) }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-12 pb-3 pt-3">
            <div class="card">
                <div class="card-header text-center bg-title">
                    <h1 class="fs-4 fw-bold">Reporte de ventas</h1>
                </div>
                <div class="card-boy">
                    <div class="table-responsive">
                        <form action="{{ route('reporteVenta') }}" class="form-group mb-2" method="GET">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="inicio">Mes</label>
                                        <input type="month" class="form-control" id="inicio" name="inicio" placeholder="Ingresar Codigo">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for=""></label>
                                       <button type="submit" class="btn btn-primary form-control"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table class="table table-bordered table-hover dataTables-productos text-center">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th>Descuento</th>
                                    <th>% de descuento</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ventas as $venta)
                                <tr>
                                   <td>{{ $venta->created_at }}</td>
                                   <td>$ {{ $venta->total }}</td>
                                   <td>$ {{ $venta->descuento}}</td>
                                   <td>{{ $venta->porcentaje}}%</td>
                                   <td><button type="button" class="btn btn-outline-dark edit" data-toggle="modal" data-target="#EditModal" data-id="{{ $venta->id }}"><i class="fas fa-edit"></i> Detalle</button></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th>Descuento</th>
                                    <th>% de descuento</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-end"></div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Productos</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover text-center" id="DetalleTabla">
                                    <thead>
                                        <th>Producto</th>
                                        <th>Precio</th>
                                        <th>Cantidad</th>
                                    </thead>
                                    <tbody id="tbody">

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Precio</th>
                                            <th>Cantidad</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
   <script src="{{ asset('js/datatables.min.js') }}"></script>
   <script>
       $(document).ready(function(){
        $('.dataTables-productos').DataTable();
        $('.edit').click(function(event){
            event.preventDefault();
            $('#tbody').html('<tr></tr>')
            let _id = $(this).data('id');
            $.ajax({
                url:'{{ url('venta') }}/'+_id,
                type: 'GET',
                dataType: 'json',
            }).done(function(data){
                for(let i = 0; i < data.length; i ++){
                    $("#DetalleTabla").append("<tr><td>" + data[i].producto.nombre + "</td><td>"+data[i].producto.precio+"</td><td>"+data[i].cantidad+"</td></tr>");
                }
            })
        });
    });
   </script>
@endpush
@endsection
