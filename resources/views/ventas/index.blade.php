@extends('layout\app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
@endpush

@section('header')
    <div class="mx-5">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-start">
                    <li class="breadcrumb-item">
                        <a href="/">Inicio</a>
                    </li>
                    <li class="breadcrumb-item active">
                    Ventas <i class="fas fa-cash-register"> </i>
                    </li>
                </ol>
            </div>
            <div class="col-sm-6 text-end mt-3">
                <a href="/"><button class="btn btn-outline-dark btn-lg"> <i class="fas fa-arrow-left"></i> Regresar</button></a>
            </div>
        </div>
        <hr>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9 pb-3 pt-3">
            <div class="card">
                <div class="card-header text-center bg-title">
                    <h1 class="fs-4 fw-bold">Ventas</h1>
                </div>
                <div class="card-body">
                    <div class="col-12 col-md-12">
                        <form action="{{ route('buscarProductoVenta') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="codigo">Código de barras</label>
                                <input id="codigo" autocomplete="off" required autofocus name="codigo" type="text" class="form-control" placeholder="Código de barras">
                            </div>
                        </form>
                        @if (Session::has('producto'))
                            <form class="form-group" action="{{ route('agregarProductoVenta') }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="codigo">Código de barras</label>
                                            <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Ingresar codigo de barras" value="{{ session('producto')->codigo }}" disabled>
                                            <input type="text" class="form-control" id="id" name="id" value="{{ session('producto')->id }}" hidden>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" class="form-control" id="nombre" name="nombre" value="{{ session('producto')->nombre }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="precio">Precio Unitario</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control" id="precio" name="precio" value="{{ session('producto')->precio }}" disabled>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="cantidad">Cantidad</label>
                                            <input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad a registrar" autofocus>
                                        </div>
                                    </div>
                                    <div class="mt-3 text-end">
                                        <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Agregar</button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                    @if (Session::has('productosVenta'))
                        <div class="col-md-12 mt-5 border-top border-1 pt-3 border-dark">
                            <div class="table-responsive">
                                <table class="table dataTables-productos">
                                    <thead>
                                        <tr>
                                            <th>Código de barras</th>
                                            <th>Descripción</th>
                                            <th>Precio</th>
                                            <th>Cantidad</th>
                                            <th>Quitar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(session("productosVenta") as $producto)
                                            <tr>
                                                <td>{{ $producto->codigo }}</td>
                                                <td>{{ $producto->nombre }}</td>
                                                <td>$ {{ number_format($producto->precio, 2) }}</td>
                                                <td>{{ $producto->existencia }}</td>
                                                <td>
                                                    <form action="{{route("quitarProductoVenta")}}" method="post">
                                                        @method("delete")
                                                        @csrf
                                                        <input type="hidden" name="indice" value="{{$loop->index}}">
                                                        <button type="submit" class="btn btn-danger">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if (Session::has('productosVenta'))
            <div class="col-md-3 pb-3 pt-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('venta.store') }}" method="POST" class="form-group">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="descuento">% de descuento</label>
                                        <input type="text" class="form-control" id="descuento" name="descuento" placeholder="Ingresar descuento">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="pago">Cantidad de pago</label>
                                        <input type="text" class="form-control" id="pago" name="pago" placeholder="Ingresar cantidad de dinero">
                                    </div>
                                </div>
                                <div class="col-md-12 text-end">
                                    <h4>Subtotoal: ${{number_format($total, 2)}}</h4>
                                    <input type="text" name="subtotal" id="subtotal" hidden value="{{number_format($total, 2)}}">
                                </div>
                                <div class="col-md-12 text-end">
                                    <h4 id="des">Descuento:</h4>
                                    <input type="text" id="desc" name="desc" hidden>
                                </div>
                                <div class="col-md-12 text-end">
                                    <h4 id="cambio">Cambio:</h4>
                                </div>
                                <div class="col-md-12 text-end">
                                    <h2 id="total">Total: </h2>
                                    <input type="text" value="" id="to" hidden>
                                </div>
                                <div class="col-md-12 text-end">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Terminar registro</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    </div>
    @push('scripts')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dataTables-productos').DataTable();
            $('#descuento').blur(function(){
                let cantidad = $('#subtotal').val();
                let porcentaje = $('#descuento').val();
                let descuento = ((porcentaje/ 100) * cantidad).toFixed(2);
                $('#des').text('Descuento: $'+descuento)
                $('#desc').val(descuento);
                let _total = cantidad - descuento;
                $('#total').text('Total: $'+ _total);
                $('#to').val(_total)
            });
            $('#pago').blur(function(){
                let _total = 0;
                let _cantidad = $('#pago').val();
                if($('#to').val() == ""){
                    let _total = $('#subtotal').val();
                }else{
                    let _total = $('#to').val();
                }
                let _cambio = _cantidad - _total;
                $('#cambio').text('Cambio: $'+_cambio);
            });
        });
    </script>
    @endpush
@endsection
